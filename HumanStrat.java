import java.util.*;
import java .io.*;
import java.util.concurrent.*;

public class HumanStrat {

	static Candidates[][] cMatrix = new Candidates[9][9];

	public static void main(String[] args) {

			HumanStrat h = new HumanStrat();

	int grid1701[][] = {
	{ 0, 0, 0, 1, 0, 0, 9, 0, 0 },
	{ 6, 0, 0, 0, 0, 3, 0, 0, 0 },
	{ 0, 2, 0, 0, 0, 0, 4, 0, 0 },
	{ 0, 9, 0, 2, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 3, 0 },
	{ 0, 0, 1, 0, 0, 6, 0, 0, 7 },
	{ 0, 0, 0, 9, 0, 0, 2, 0, 0 },
	{ 3, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 7, 0, 0, 0, 0, 0, 0, 0, 8 } };


			h.initCandidates(grid1701);
			h.solveSudoku(grid1701);
	}


	public void initCandidates(int[][] grid) {
		for(int j = 0; j<9; j++) {
			for(int i = 0; i<9; i++) {
				if(grid[i][j] == 0) {
					cMatrix[i][j] = new Candidates();
					removeCandidates(grid,i,j);
				}
			}
		}

	}

	public void removeCandidates(int[][] grid, int i, int j) {

		for(int col = 0; col < 9; col++) {
			if(cMatrix[i][j].candidateList.contains(grid[i][col])) {
				int index = cMatrix[i][j].candidateList.indexOf(grid[i][col]);
				cMatrix[i][j].candidateList.remove(index);
			}
		}

		for(int row = 0; row < 9; row++) {
			if(cMatrix[i][j].candidateList.contains(grid[row][j])) {
				int index = cMatrix[i][j].candidateList.indexOf(grid[row][j]);
				cMatrix[i][j].candidateList.remove(index);
			}	
		}
	

		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (cMatrix[i][j].candidateList.contains(grid[row + (i-i%3)][col + (j-j%3)])) {
					int index = cMatrix[i][j].candidateList.indexOf(grid[row + (i-i%3)][col + (j-j%3)]);
					cMatrix[i][j].candidateList.remove(index);
				}
			}
		}

	}

	public boolean solveSudoku(int[][] grid ) {
		
		//uniqueMissing(grid);
		//nakedSingles(grid);
		//hiddenSingles(grid);
		/*int iter = 0;
		while(insertUniqueMissing(grid) > 0) {
			iter++;
		}*/

		BacktrackingHuman b = new BacktrackingHuman();
		long start = System.nanoTime();
		b.solveSudoku(grid, cMatrix);
		long totalTime = System.nanoTime() - start;
		System.out.println(("BACKTRACKING MED CAND " + TimeUnit.NANOSECONDS.toMillis(totalTime/99)) + "ms");

	int grid1701[][] = {
	{ 0, 0, 0, 1, 0, 0, 9, 0, 0 },
	{ 6, 0, 0, 0, 0, 3, 0, 0, 0 },
	{ 0, 2, 0, 0, 0, 0, 4, 0, 0 },
	{ 0, 9, 0, 2, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 0, 0, 0, 0, 3, 0 },
	{ 0, 0, 1, 0, 0, 6, 0, 0, 7 },
	{ 0, 0, 0, 9, 0, 0, 2, 0, 0 },
	{ 3, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 7, 0, 0, 0, 0, 0, 0, 0, 8 } };


		Backtracking bt = new Backtracking();
		long start1 = System.nanoTime();
		bt.solveSudoku(grid1701);
		long totalTime1 = System.nanoTime() - start1;
		System.out.println(("BACKTRACKING " + TimeUnit.NANOSECONDS.toMillis(totalTime1/99)) + "ms");


		return true;

	}

	public int insertUniqueMissing(int[][] grid) {

		int inserted = 0;
		
		for(int j = 0; j < 9; j++) {
			for(int i = 0; i < 9; i++) {
				if(grid[i][j] == 0 && cMatrix[i][j].candidateList.size() == 1) {
					inserted++;
					grid[i][j] = cMatrix[i][j].candidateList.get(0);
					removeInsertedCandidate(grid, i, j, grid[i][j]);
				}
			}
		}
		return inserted;
	}
 
 	public void removeInsertedCandidate(int[][] grid, int i, int j, int num) {

		for(int col = 0; col < 9; col++) {
			if(grid[i][col] == 0 && cMatrix[i][col].candidateList.contains(num)) {
				int index = cMatrix[i][col].candidateList.indexOf(num);
				if(index > (-1))
					cMatrix[i][col].candidateList.remove(index);
			}
		} 

		for(int row = 0; row < 9; row++) {
			if(grid[row][j] == 0 && cMatrix[row][j].candidateList.contains(num)) {
				int index = cMatrix[row][j].candidateList.indexOf(num);
				if(index > (-1))
					cMatrix[row][j].candidateList.remove(index);
				
			}

		}

		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (grid[row + (i-i%3)][col + (j-j%3)] == 0 && cMatrix[row + (i-i%3)][col + (j-j%3)].candidateList.contains(num)) {
					int index = cMatrix[row + (i-i%3)][col + (j-j%3)].candidateList.indexOf(num);
					if(index > (-1))
						cMatrix[row + (i-i%3)][col + (j-j%3)].candidateList.remove(index);
				}
			}
		}						

 	}

	public void uniqueMissingInRow(int[][] grid, int row) {
		
		int count = 0;
		int index = 0;

		ArrayList<Integer> list = new ArrayList();

		for(int i = 0; i < 9; i++) 
			list.add(i+1);
		
		for(int i = 0; i < 9; i++) 
			System.out.println(list.get(i));
		

		for(int i = 0; i < 9; i++) {
			if(grid[row][i] == 0) {
				count++;
				index = i;
			} else {
				int toRemove = list.indexOf(grid[row][i]);
				System.out.println(grid[row][i] + " " + "row: " + row + " column: " + i);
				list.remove(toRemove);
			}
		}
		
		if(count == 1) 
			grid[row][index] = list.get(0);
		
	}	

	public void uniqueMissingInCol(int[][] grid, int col) {

		int count = 0;
		int index = 0;

		ArrayList<Integer> list = new ArrayList();

		for(int i = 0; i < 9; i++) 
			list.add(i+1);
		
		for(int i = 0; i < 9; i++) 
			System.out.println(list.get(i));
		

		for(int i = 0; i < 9; i++) {
			if(grid[i][col] == 0) {
				count++;
				index = i;
			} else {
				int toRemove = list.indexOf(grid[i][col]);
				list.remove(toRemove);
			}
		}
		
		if(count == 1) 
			grid[index][col] = list.get(0);
	}	




	public void nakedSingles() {

	}






	public void printGrid(int grid[][]) {
		for (int row = 0; row < grid.length; row++) {
			for (int col = 0; col < grid.length; col++)
				System.out.printf("%2d", grid[row][col]);
			System.out.printf("\n");
		}
	}



}