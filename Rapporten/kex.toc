\select@language {swedish}
\contentsline {section}{\numberline {1}Terminologi}{5}
\contentsline {section}{\numberline {2}Introduktion}{6}
\contentsline {section}{\numberline {3}Bakgrund}{7}
\contentsline {subsection}{\numberline {3.1}Tidigare forskning}{7}
\contentsline {subsubsection}{\numberline {3.1.1}Backtracking}{7}
\contentsline {subsubsection}{\numberline {3.1.2}Villkorsprogrammering}{7}
\contentsline {subsubsection}{\numberline {3.1.3}M\IeC {\"a}nskliga strategier}{8}
\contentsline {subsection}{\numberline {3.2}Syfte}{8}
\contentsline {subsection}{\numberline {3.3}Problemformulering}{9}
\contentsline {subsubsection}{\numberline {3.3.1}Avgr\IeC {\"a}nsningar}{9}
\contentsline {section}{\numberline {4}Metod}{10}
\contentsline {subsection}{\numberline {4.1}Programmeringsspr\IeC {\r a}k}{10}
\contentsline {subsection}{\numberline {4.2}Sudokul\IeC {\"o}sare med backtracking}{10}
\contentsline {subsection}{\numberline {4.3}Sudokul\IeC {\"o}sare med m\IeC {\"a}nskliga strategier och backtracking}{11}
\contentsline {subsection}{\numberline {4.4}Testdata}{12}
\contentsline {section}{\numberline {5}Resultat}{13}
\contentsline {subsection}{\numberline {5.1}Backtracking}{13}
\contentsline {subsection}{\numberline {5.2}M\IeC {\"a}nskliga strategier}{13}
\contentsline {subsection}{\numberline {5.3}J\IeC {\"a}mf\IeC {\"o}relse av k\IeC {\"o}rtid}{14}
\contentsline {section}{\numberline {6}Analys \& Diskussion}{16}
\contentsline {subsection}{\numberline {6.1}Metodkritik}{17}
\contentsline {section}{\numberline {7}Slutsats}{18}
