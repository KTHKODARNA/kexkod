import java.util.*;
import java .io.*;
import java.util.concurrent.*;

public class Backtracking {

/**
* @param args
*
* TODO: 
*
* Fixa massa testfall. 17,18,19,20 clues (1 TIMME)
* Timea. (30 min)
* Fixa grafer på körtider (1 TIMME)
* Skriva om komplexitet och tiderna. (4 TIMMAR)
* 
*/

static ArrayList<int[][]> clues = new ArrayList();
static long anrop = 0L;


public static void main(String[] args) {
	
	Testfall test = new Testfall();
	Backtracking problem = new Backtracking();
	for(int j = 0; j < 1; j++) {
		/*switch(j) {
			case 0: 
				test.load17();
				break;
			case 1:
				test.load18();
				break;
			case 2:
				test.load19();
				break;
			case 3:
				test.load20();
				break;	
			case 4:
				test.load21();
				break;						
		}*/
		test.load17();
		anrop = 0L;
		clues = test.allTests.get(j);
		System.out.println((17+j) + " clues");
		long totalTime = 0L;

		for(int i = 0; i < 99; i++) {
			long startTime = System.nanoTime();
			long estimatedTime = 0L;
			int grid[][] = clues.get(i);
			if (problem.solveSudoku(grid)) {
				estimatedTime = System.nanoTime() - startTime;
				//System.out.println("PUSLET: "+ anrop/(i+1));
				//problem.printGrid(grid);
				//System.out.println("KÖRTID: ");
				//System.out.print(TimeUnit.NANOSECONDS.toMillis(estimatedTime) + ", ");
				totalTime = totalTime + estimatedTime;
			}
		}
		System.out.println("SNITT ANROP: " + anrop/100);
		System.out.println((TimeUnit.NANOSECONDS.toMillis(totalTime/99)) + "ms");
	


	}
}

public boolean solveSudoku(int[][] grid) {
	anrop++;
	Point point = new Point(0, 0);
	if (!findUnassignedLocation(grid, point)) {
		anrop--;
		System.out.println("ANROP MED BACKTRACK " + anrop);

		return true;
	}

	for (int num = 1; num <= 9; num++) {
		if (isSafe(grid, point.row, point.col, num)) {
			grid[point.row][point.col] = num;

			if (solveSudoku(grid)) {
				return true;
			}
	
			grid[point.row][point.col] = 0;
		}
	}
	return false;
}

/**
* Searches the grid to find an entry that is still unassigned. If found,
* the reference parameters row, col will be set the location that is
* unassigned, and true is returned. If no unassigned entries remain, false
* is returned.
* 
**/
private boolean findUnassignedLocation(int grid[][], Point point) {
for (int row = 0; row < grid.length; row++)
	for (int col = 0; col < grid.length; col++)
		if (grid[row][col] == 0) {
			point.row = row;
			point.col = col;
			return true;
		}

return false;
}

/**
* Returns a boolean which indicates whether it will be legal to assign num
* to the given row,col location.
**/
boolean isSafe(int grid[][], int row, int col, int num) {
return !usedInRow(grid, row, num) &&
	   !usedInCol(grid, col, num) && 
	   !usedInBox(grid, row - row % 3, col - col % 3, num);
}

/**
* Returns a boolean which indicates whether any assigned entry in the
* specified row matches the given number.
* 
**/
boolean usedInRow(int grid[][], int row, int num) {
	for (int col = 0; col < grid.length; col++)
		if (grid[row][col] == num)
			return true;
	return false;
}

/**
* Returns a boolean which indicates whether any assigned entry in the
* specified column matches the given number.
**/
boolean usedInCol(int grid[][], int col, int num) {
	for (int row = 0; row < grid.length; row++)
		if (grid[row][col] == num)
			return true;
	return false;
}

/**
* 
* @param grid
* @param cellRow
* @param cellCol
* @param num
* @return
*/
private boolean usedInBox(int[][] grid, int cellRow, int cellCol, int num) {
	for (int row = 0; row < 3; row++)
		for (int col = 0; col < 3; col++) {
			if (grid[row + cellRow][col + cellCol] == num)
				return true;
		}
	return false;
}

/**
* A utility function to print grid
* 
* @param grid
*/
public void printGrid(int grid[][]) {
	for (int row = 0; row < grid.length; row++) {
		for (int col = 0; col < grid.length; col++)
			System.out.printf("%2d", grid[row][col]);
		System.out.printf("\n");
	}
}
}