import java.util.*;
import java .io.*;
import java.util.concurrent.*;

public class BacktrackingHuman {

Candidates[][] cMatrix = new Candidates[9][9];
int grid1701[][] = {
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ 0, 0, 0, 3, 8, 0, 0, 0, 9 },
	{ 0, 5, 0, 0, 0, 1, 0, 0, 0 },
	{ 0, 0, 7, 0, 0, 6, 0, 0, 0 },
	{ 8, 0, 0, 0, 0, 5, 0, 0, 0 },
	{ 9, 0, 5, 0, 0, 0, 2, 0, 1 },
	{ 0, 0, 1, 2, 0, 0, 0, 0, 7 },
	{ 0, 0, 0, 9, 0, 0, 8, 0, 0 },
	{ 0, 6, 0, 0, 0, 0, 0, 5, 0 } };


public BacktrackingHuman() {
	long start = System.nanoTime();
	initCandidates(grid1701);
	solveSudoku(grid1701);
	long totalTime = System.nanoTime() - start;
	System.out.println(("Backtracking med cand: " + TimeUnit.NANOSECONDS.toMillis(totalTime/99)) + "ms\n");
	printGrid(grid1701);
}

long anrop = 0l;

public static void main(String[] args) {
	BacktrackingHuman b = new BacktrackingHuman();
}


public boolean solveSudoku(int[][] grid) {

	anrop++;
	Point point = new Point(0, 0);
	if (!findUnassignedLocation(grid, point)) {
		anrop--;
		System.out.println("\nAnrop med Human (Success): " + anrop);
		return true;
	}
	
	int iter = cMatrix[point.row][point.col].candidateList.size();

	for (int num = 0; num < iter ; num++) {
		int candNum = cMatrix[point.row][point.col].candidateList.get(num);
		if (isSafe(grid, point.row, point.col, candNum)) {
	
			grid[point.row][point.col] = candNum;
			if (solveSudoku(grid)) {
				return true;
			}
			grid[point.row][point.col] = 0;
		}
		
	}
	return false;	
}

public void initCandidates(int[][] grid) {
	for(int j = 0; j<9; j++) {
		for(int i = 0; i<9; i++) {
			if(grid[i][j] == 0) {
				cMatrix[i][j] = new Candidates();
				removeCandidates(grid,i,j);
			}
		}
	}
}

public void removeCandidates(int[][] grid, int i, int j) {

	for(int col = 0; col < 9; col++) {
		if(cMatrix[i][j].candidateList.contains(grid[i][col])) {
			int index = cMatrix[i][j].candidateList.indexOf(grid[i][col]);
			cMatrix[i][j].candidateList.remove(index);
		}
	}

	for(int row = 0; row < 9; row++) {
		if(cMatrix[i][j].candidateList.contains(grid[row][j])) {
			int index = cMatrix[i][j].candidateList.indexOf(grid[row][j]);
			cMatrix[i][j].candidateList.remove(index);
		}	
	}


	for (int row = 0; row < 3; row++) {
		for (int col = 0; col < 3; col++) {
			if (cMatrix[i][j].candidateList.contains(grid[row + (i-i%3)][col + (j-j%3)])) {
				int index = cMatrix[i][j].candidateList.indexOf(grid[row + (i-i%3)][col + (j-j%3)]);
				cMatrix[i][j].candidateList.remove(index);
			}
		}
	}
}

public void removeInsertedCandidate(int[][] grid, int i, int j, int num) {

		for(int col = 0; col < 9; col++) {
			if(grid[i][col] == 0 && cMatrix[i][col].candidateList.contains(num)) {
				int index = cMatrix[i][col].candidateList.indexOf(num);
				if(index > (-1))
					cMatrix[i][col].candidateList.remove(index);
			}
		}

		for(int row = 0; row < 9; row++) {
			if(grid[row][j] == 0 && cMatrix[row][j].candidateList.contains(num)) {
				int index = cMatrix[row][j].candidateList.indexOf(num);
				if(index > (-1))
					cMatrix[row][j].candidateList.remove(index);
				
			}

		}

		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (grid[row + (i-i%3)][col + (j-j%3)] == 0 && cMatrix[row + (i-i%3)][col + (j-j%3)].candidateList.contains(num)) {
					int index = cMatrix[row + (i-i%3)][col + (j-j%3)].candidateList.indexOf(num);
					if(index > (-1))
						cMatrix[row + (i-i%3)][col + (j-j%3)].candidateList.remove(index);
				}
			}
		}
 	}
/**
* Searches the grid to find an entry that is still unassigned. If found,
* the reference parameters row, col will be set the location that is
* unassigned, and true is returned. If no unassigned entries remain, false
* is returned.
* 
**/
private boolean findUnassignedLocation(int grid[][], Point point) {
for (int row = 0; row < grid.length; row++)
	for (int col = 0; col < grid.length; col++)
		if (grid[row][col] == 0) {
			point.row = row;
			point.col = col;
			return true;
		}

return false;
}

/**
* Returns a boolean which indicates whether it will be legal to assign num
* to the given row,col location.
**/
boolean isSafe(int grid[][], int row, int col, int num) {
return !usedInRow(grid, row, num) &&
	   !usedInCol(grid, col, num) && 
	   !usedInBox(grid, row - row % 3, col - col % 3, num);
}

/**
* Returns a boolean which indicates whether any assigned entry in the
* specified row matches the given number.
* 
**/
boolean usedInRow(int grid[][], int row, int num) {
	for (int col = 0; col < grid.length; col++)
		if (grid[row][col] == num)
			return true;
	return false;
}

/**
* Returns a boolean which indicates whether any assigned entry in the
* specified column matches the given number.
**/
boolean usedInCol(int grid[][], int col, int num) {
	for (int row = 0; row < grid.length; row++)
		if (grid[row][col] == num)
			return true;
	return false;
}


private boolean usedInBox(int[][] grid, int cellRow, int cellCol, int num) {
	for (int row = 0; row < 3; row++)
		for (int col = 0; col < 3; col++) {
			if (grid[row + cellRow][col + cellCol] == num)
				return true;
		}
	return false;
}

/**
* A utility function to print grid
* 
* @param grid
*/
public void printGrid(int grid[][]) {
	for (int row = 0; row < grid.length; row++) {
		for (int col = 0; col < grid.length; col++)
			System.out.printf("%2d", grid[row][col]);
			System.out.printf("\n");
		}
	}
}